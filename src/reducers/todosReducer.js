import { ADD_TODO, DELETE_TODO, TOGGLE_COMPLETED_TODO } from '../actions/actions'

const initialState = {
  list: []
}

export const todosReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_TODO:
      return {
        list: [...state.list, {
          ...action.payload,
          id: +new Date(),
          completed: false
        }]
      }
    case DELETE_TODO:
      return {
        list: state.list.filter(todo => todo.id !== action.payload.id)
      }
    case TOGGLE_COMPLETED_TODO:
      return {
        list: state.list.map(todo => {
          if (todo.id === action.payload.id) {
            return {
              ...todo,
              completed: !todo.completed
            }
          }
          return todo
        })
      }
    default:
      return state
  }
}
