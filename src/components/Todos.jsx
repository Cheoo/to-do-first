import React, { useMemo, useState } from 'react'
import { useSelector } from 'react-redux'
import Todo from './Todo'

const VALUES = {
  all: 'all',
  active: 'active',
  completed: 'completed'
}

function Todos () {
  const todos = useSelector(state => state.todos.list)
  const [value, setValue] = useState(VALUES.all)

  const filteredItems = useMemo(() => {
    switch (value) {
      case VALUES.active:
        return todos.filter(todo => todo.completed === false)
      case VALUES.completed:
        return todos.filter(todo => todo.completed === true)
      default:
        return todos
    }
  }, [todos, value])

  const changeValue = e => {
    setValue(e.target.value)
  }

  return (
        <div className='todos'>
            <div className='filter'>
                <label>
                    <p>all</p>
                    <input
                        type="radio"
                        name='filter'
                        value="all"
                        checked={value === VALUES.all}
                        onChange={changeValue}/>
                </label>
                <label>
                    <p>active</p>
                    <input
                        type="radio"
                        name='filter'
                        value="active"
                        checked={value === VALUES.active}
                        onChange={changeValue}/>
                </label>
                <label>
                    <p>completed</p>
                    <input
                        type="radio"
                        name='filter'
                        value="completed"
                        checked={value === VALUES.completed}
                        onChange={changeValue}/>
                </label>
            </div>
            {
                filteredItems.map((todo) => {
                  return (
                      <Todo todo={todo} key={todo.id} />
                  )
                })
            }
        </div>
  )
}

export default Todos
