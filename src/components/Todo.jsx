import React from 'react'
import { useDispatch } from 'react-redux'
import { actionDeleteTodo, actionToggleCompletedTodo } from '../actions/actions'

function Todo ({ todo }) {
  const dispatch = useDispatch()

  const removeTodo = () => {
    dispatch(actionDeleteTodo({ id: todo.id }))
  }

  const toggleCompletedTodo = () => {
    dispatch(actionToggleCompletedTodo({ id: todo.id }))
  }
  return (
        <div className='todo' style={{ textDecoration: todo.completed ? 'line-through' : 'none' }} onClick={toggleCompletedTodo}>
            <div className='todo__wrap'>
                <div className='todo__title'><h3>{todo.title}</h3></div>
                <div className='todo__desc'>
                    <p>
                        {todo.desc}
                    </p>
                </div>
            </div>
            <button className='delete' onClick={removeTodo}>x</button>
        </div>
  )
}

export default Todo
