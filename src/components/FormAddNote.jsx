import React, { useCallback } from 'react'
import { useDispatch } from 'react-redux'
import { actionAddTodo } from '../actions/actions'

function FormAddNote () {
  const dispatch = useDispatch()

  const addTodo = (form) => {
    const formData = new FormData(form)

    dispatch(actionAddTodo({
      title: formData.get('title'),
      desc: formData.get('desc')
    }))

    form.reset()
  }

  const submitForm = useCallback(e => {
    e.preventDefault()
    addTodo(e.target)
  })

  return (
    <form onSubmit={submitForm}>
      <label>
        <h3>Заголовок</h3>
        <input name='title' type="text"/>
      </label>
      <label>
        <h3>Описание</h3>
        <textarea name="desc" cols="30" rows="10"></textarea>
      </label>
      <div className='footer-form'>
        <input type="submit" value="Добавить заметку"/>
      </div>
    </form>
  )
}

export default FormAddNote
