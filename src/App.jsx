import React from 'react'
import './App.css'
import FormAddNote from './components/FormAddNote'
import Todos from './components/Todos'

function App () {
  return (
    <div className="App">
      <FormAddNote />
      <Todos />
    </div>
  )
}

export default App
