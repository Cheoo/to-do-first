export const ADD_TODO = 'ADD_TODO'
export const DELETE_TODO = 'DELETE_TODO'
export const TOGGLE_COMPLETED_TODO = 'TOGGLE_COMPLETED_TODO'

export const actionAddTodo = payload => ({ type: ADD_TODO, payload })
export const actionDeleteTodo = payload => ({ type: DELETE_TODO, payload })
export const actionToggleCompletedTodo = payload => ({ type: TOGGLE_COMPLETED_TODO, payload })
